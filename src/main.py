from pdf_converter import get_pdf
from path_checker import check_paths
from pdf_writer import write_pdf


def main(input_files: list[str], output_file: str) -> str:
    check_paths(input_files)
    pdf = get_pdf(input_files)
    result = write_pdf(pdf, output_file)
    return result


if __name__ == "__main__":
    import sys, os

    input_files = sys.argv[1:-1]
    output_file = sys.argv[-1]
    for i in range(len(input_files)):
        no_user = os.path.expanduser(input_files[i])
        abs_path = os.path.abspath(no_user)
        input_files[i] = abs_path
    res = main(input_files, output_file)
    print(res, end="", flush=True)