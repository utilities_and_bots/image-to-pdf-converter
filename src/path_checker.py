def check_paths(paths: list[str]) -> bool:
    accepted_extensions = ("png", "jpeg", "jpg", "pdf", "bmp")
    res = True
    for x in paths:
        if not x.lower().endswith(accepted_extensions):
            res = False
            break
    return res