import pypdf
import img2pdf
import os
import random


def add_image(doc: pypdf.PdfWriter, path: str) -> pypdf.PdfWriter:
    temp_path = "/tmp/temImagePdf" + str(random.randint(1, 1000000000)) + ".pdf"
    f = open(temp_path, "wb")
    dpix = dpiy = 300
    layout_fun = img2pdf.get_fixed_dpi_layout_fun((dpix, dpiy))
    f.write(img2pdf.convert(path, layout_fun=layout_fun))  # type: ignore
    f.close()
    doc = add_pdf(doc, temp_path)
    os.remove(temp_path)
    return doc


def add_pdf(doc: pypdf.PdfWriter, path: str) -> pypdf.PdfWriter:
    new_pdf = pypdf.PdfReader(path)
    doc.append(new_pdf)
    return doc


def is_image(path: str) -> bool:
    return path.lower().endswith((".png", ".jpeg", ".jpg", ".bmp"))


def get_pdf(input_files: list[str]) -> pypdf.PdfWriter:
    doc = pypdf.PdfWriter()
    for x in input_files:
        if is_image(x):
            doc = add_image(doc, x)
        else:
            doc = add_pdf(doc, x)
    return doc
