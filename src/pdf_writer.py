import pypdf


def write_pdf(pdf: pypdf.PdfWriter, path: str) -> str:
    pdf.write(path)
    return path
